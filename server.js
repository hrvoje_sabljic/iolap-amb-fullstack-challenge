const express = require('express');
const path = require('path');
const app = express();
const port = process.env.PORT || 5000;
const bodyParser = require('body-parser');
const models = require('./src/models');
const winston = require('./logger/winston');

process.env.SECRET_KEY = 'jwt_secret';
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// API register
const userRouter = require('./src/routes/user');
app.use('/users', userRouter);
const orderRouter = require('./src/routes/order');
app.use('/orders', orderRouter);
const pricingRouter = require('./src/routes/pricing');
app.use('/pricing', pricingRouter);

if (process.env.NODE_ENV !== 'production') {
  models.sequelize.sync().then(() => {
    winston.info('DB initialized!');
  });
}
if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'client/build')));
  // Handle React routing, return all requests to React app
  app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}
app.listen(port, () => {
  winston.info(`Listening on port ${port}`);
});

process.on('uncaughtException', (err) => {
  winston.error(err);
})
//For testing purposes app needs to be exported
module.exports = app;
