const expect = require('chai').expect;
const models = require('../../src/models');
const Pricing = models.Pricing;
describe('Pricing model create and update tests', () => {
  beforeEach((done) => {
    models.sequelize.sync({
      'force': true
    }).then(() => {
      done()
    });

  });

  it('should not allow create more than 1 record', (done) => {
    Pricing.create({
      ik: '',
      basePrice: 4.0,
      additionalMileCost: 1.0
    }).then(res => {
      pricing = res
      Pricing.create({
        ik: '',
        basePrice: 5.0,
        additionalMileCost: 1.0
      }).then(res => {
        expect(res).to.be.be(null);
        done();
      }).catch(ex => {
        expect(ex.name).to.be.equal('SequelizeUniqueConstraintError')
        done();
      })
    }).catch(ex => done(ex))
  })

  it('should modify existing record', (done) => {
    Pricing.update({
      basePrice: 10.0,
      additionalMileCost: 2.0
    }, {
      where: {
        ik: ''
      }
    }).then(res => {
      expect(res.length).to.be.equal(1);
      done();
    }).catch(ex => {
      done(ex)
    })
  })
});
