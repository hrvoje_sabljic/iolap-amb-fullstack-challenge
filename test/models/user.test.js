const models = require('../../src/models');
const User = models.User;
const expect = require('chai').expect;

describe('User model build validation', () => {
  let user;

  beforeEach(() => {
    user = User.build({
      username: "Test user 1",
      password: "foobar"
    });
  });

  it('should build record correctly', () => {
    expect(user.username).to.equal("Test user 1");
  });

  it('password should be present', (done) => {
    user.password = " ".repeat(6);
    user.validate().then((errors) => {
      expect(errors).to.be.not.equal(undefined);
      done();
    }).catch(ex => {
      expect(ex.name).to.be.equal('SequelizeValidationError');
      done()
    });
  });

  it('username should be present', (done) => {
    user.username = " ".repeat(6);
    user.validate().then((errors) => {
      expect(errors).to.be.not.equal(undefined);
      done();
    }).catch(ex => {
      expect(ex.name).to.be.equal('SequelizeValidationError');
      done()
    });
  });

  it('password should have a minimum length', (done) => {
    user.password = " ".repeat(5);
    user.validate().then(function (errors) {
      assert.notEqual(errors, undefined);
      done();
    }).catch(ex => {
      expect(ex.name).to.be.equal('SequelizeValidationError');
      done()
    });
  });
});
