const models = require('../../src/models');
const Order = models.Order;
const expect = require('chai').expect;

describe('Order model build validation', () => {
  let order;

  beforeEach(() => {
    order = Order.build({
      orderStatus: "Requested",
      timeAndDate: new Date(),
      origin: "Test origin",
      destination: "test destination",
      farePrice: 123.34,
      phoneNumber: '999888777666'
    });
  });

  it('should build orderId in UUID 8-4-4-4-12 format', () => {
    let re = new RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i);
    expect(re.test(order.orderID)).to.equal(true);
  });

  it('should throw error if  destination empty', (done) => {
    order.destination = " ".repeat(6);
    order.validate().then((errors) => {
      expect(errors).to.be.not.equal(undefined);
      done();
    }).catch(ex => {
      expect(ex.name).to.be.equal('SequelizeValidationError');
      done()
    });
  });

  it('should throw error if  origin empty', (done) => {
    order.origin = " ".repeat(6);
    order.validate().then((errors) => {
      expect(errors).to.be.not.equal(undefined);
      done();
    }).catch(ex => {
      expect(ex.name).to.be.equal('SequelizeValidationError');
      done()
    });
  });

  it('should throw error if  phone number empty', (done) => {
    order.phoneNumber = " ".repeat(6);
    order.validate().then((errors) => {
      expect(errors).to.be.not.equal(undefined);
      done();
    }).catch(ex => {
      expect(ex.name).to.be.equal('SequelizeValidationError');
      done()
    });
  });

  it('should throw error if email format is incorrect', (done) => {
    order.email = "testwrong mail";
    order.validate().then((errors) => {
      expect(errors).to.be.not.equal(undefined);
      done();
    }).catch(ex => {
      expect(ex.name).to.be.equal('SequelizeValidationError');
      done()
    });
  });

  it('should throw error if email format is incorrect', (done) => {
    order.email = "test@test";
    order.validate().then((errors) => {
      expect(errors).to.be.not.equal(undefined);
      done();
    }).catch(ex => {
      expect(ex.name).to.be.equal('SequelizeValidationError');
      done()
    });
  });

  it('should throw error if email format is incorrect', (done) => {
    order.email = "test.test";
    order.validate().then((errors) => {
      expect(errors).to.be.not.equal(undefined);
      done();
    }).catch(ex => {
      expect(ex.name).to.be.equal('SequelizeValidationError');
      done()
    });
  });
});
