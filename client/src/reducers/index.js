import {
  combineReducers
} from 'redux';

import {
  authentication
} from './authentication.reducer';
import {
  alert
} from './alert.reducer';

import {
  orders,order
} from './order.reducers';

import {
  pricing
} from './pricing.reducer';

const rootReducer = combineReducers({
  authentication,
  alert,
  orders,
  pricing,
  order
});

export default rootReducer;
