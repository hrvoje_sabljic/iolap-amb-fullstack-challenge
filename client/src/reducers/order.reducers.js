import {
  orderConstants
} from '../constants';

export const orders = (state = {}, action) => {
  switch (action.type) {
    case orderConstants.GET_ALL_ORDERS:
      return {
        loading: true
      };
    case orderConstants.GET_ALL_ORDERS_SUCCESS:
      return {
        items: action.orders
      };
    case orderConstants.GET_ALL_ORDERS_FAILURE:
      return {
        error: action.error
      };
    case orderConstants.CREATE_ORDER:
      return {
        loading: true
      };
    case orderConstants.CREATE_ORDER_STATUS_SUCCESS:
      return {
        order: action.order
      };
    case orderConstants.CREATE_ORDER_STATUS_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}

export const order = (state = {}, action) => {
  switch (action.type) {
    case orderConstants.GET_ORDER:
      return {
        loading: true
      };
    case orderConstants.GET_ORDER_SUCCESS:
      return action.order;

    case orderConstants.GET_ORDER_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}
