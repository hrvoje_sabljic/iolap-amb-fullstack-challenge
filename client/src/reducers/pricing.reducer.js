import {
  pricingConstants
} from '../constants';

export const pricing = (state = {}, action) => {
  switch (action.type) {
    case pricingConstants.GET_PRICING:
      return {
        loading: true
      };
    case pricingConstants.GET_PRICING_SUCCESS:
      return  action.pricing;
    case pricingConstants.GET_PRICING_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}
