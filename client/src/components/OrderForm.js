import React from 'react';
import { connect } from 'react-redux';
import { orderActions,pricingActions} from '../actions';
import { createGeoInput, DefaultGeoInput } from 'react-geoinput';
import { Link } from 'react-router-dom';

const SimpleInput=createGeoInput(DefaultGeoInput);

class OrderForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            orderStatus:'',
            timeAndDate:'',
            phoneNumber:'',
            email:'',
            origin: '',
            destination:'',
            farePrice:0,
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { origin, destination ,phoneNumber,email,farePrice,timeAndDate} = this.state;
        const { dispatch } = this.props;
        dispatch(orderActions.create({phoneNumber:phoneNumber,email:email,origin:origin,destination:destination,farePrice:farePrice,timeAndDate:timeAndDate}));

    }

    onAddressChange=value=>{this.setState({origin:value});this.handleDistanceCalculation()};
    onGeoDestinationChange=value=>this.setState({originD:value});

    onDestinationChange=value=>{this.setState({destination:value});this.handleDistanceCalculation()};
    onGeoDestinationChange2=value=>this.setState({destinationD:value});

    handleDistanceCalculation(){
        const {pricing}=this.props;
        if(this.state.origin&&this.state.destination){
            this.gds.getDistanceMatrix({
                origins:[this.state.origin],destinations:[this.state.destination],travelMode:'DRIVING'
            },(response,status)=>{
                let result=response.rows[0].elements[0];
                if(result.hasOwnProperty('distance')===true){
                let dis=result.distance.value;
                if(dis<=3000){
                    this.setState({farePrice:pricing.basePrice*1})
                }else{
                    let totalAmount=pricing.basePrice*1;
                    dis=dis-3000;
                    totalAmount+=(Math.floor(dis/1000)*(pricing.additionalMileCost*1));
                    this.setState({farePrice:totalAmount})
                }}
            })
        }
    }

    componentDidMount() {
        this.gds=new window.google.maps.DistanceMatrixService();
        this.props.dispatch(pricingActions.get());
      }

    render() {
        const {submitted,farePrice, email ,phoneNumber,origin,destination,timeAndDate} = this.state;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2 style={{display:'inline-block'}}>Order taxi</h2><h4 style={{display:'inline-block',float:'right'}}> <Link to="/orders" >Orders list</Link></h4>
                <form name="form" onSubmit={this.handleSubmit}>
                <div className={'form-group' + (submitted && !origin ? ' has-error' : '')}>
                        <label htmlFor="origin">Pickup location</label>
                       <SimpleInput addressInput={{onChange:this.onAddressChange,
                                                   value:this.state.origin,}}
                                                   geoDestinationInput={{
                                                    onChange: this.onGeoDestinationChange,
                                                    value: this.state.geoDestination,
                                                  }} />
                        {submitted && !origin &&
                            <div className="help-block">Pickup location is required</div>
                        }
                    </div>

                <div className={'form-group' + (submitted && !destination ? ' has-error' : '')}>
                        <label htmlFor="destination">Destination address</label>
                       <SimpleInput addressInput={{onChange:this.onDestinationChange,
                                                   value:this.state.destination,}}
                                                   geoDestinationInput={{
                                                    onChange: this.onGeoDestinationChange2,
                                                    value: this.state.geoDestination,
                                                  }} />
                        {submitted && !destination &&
                            <div className="help-block">Destination is required</div>
                        }
                    </div>

                    <div className={'form-group' + (submitted && !timeAndDate ? ' has-error' : '')}>
                        <label htmlFor="timeAndDate">Time and date of pickup</label>
                        <input type="datetime-local" className="form-control" name="timeAndDate" value={timeAndDate} onChange={this.handleChange} />
                        {submitted && !timeAndDate &&
                            <div className="help-block">Time is required</div>
                        }
                    </div>

                    <div className={'form-group' + (submitted && !email ? ' has-error' : '')}>
                        <label htmlFor="email">Email</label>
                        <input type="email" className="form-control" name="email" value={email} onChange={this.handleChange} />
                        {submitted && !email &&
                            <div className="help-block">email is required</div>
                        }
                    </div>

                    <div className={'form-group' + (submitted && !phoneNumber ? ' has-error' : '')}>
                        <label htmlFor="phoneNumber">Phone number</label>
                        <input type="text" className="form-control" name="phoneNumber" value={phoneNumber} onChange={this.handleChange} />
                        {submitted && !phoneNumber &&
                            <div className="help-block">phone number is required</div>
                        }
                    </div>

                    <div className="form-group">
                        <button className="btn btn-primary">Order</button>
                    </div>
                </form>
                Fare price: <label>{farePrice}$</label>
            </div>
    );
    }
}

const mapStateToProps=(state)=> {
    const { pricing } = state;
    return {
        pricing
    };
}

const connectedOrderForm = connect(mapStateToProps)(OrderForm);
export { connectedOrderForm as OrderForm };
