import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

import { orderActions } from '../actions';

const statusType={
'Requested':'Requested',
'Declined':'Declined',
'In progress':'In progress',
'Done':'Done'
}

const enumFormatter=(cell,row,enumObject)=>{
    return enumObject[cell];
}
const statusSelectionValues=['Requested','Declined','In progress','Done'];

const linkFormatter=(cell)=>{
    let path=`/orders/${cell}`;
    return (<Link to={path}>View</Link>);
}

const currencyFormatter=(cell)=>{
    let c=cell+'$'
    return (c);
}

class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.onAfterSaveCell = this.onAfterSaveCell.bind(this);
    }

    onAfterSaveCell(row){
        this.props.dispatch(orderActions.update(row));
    }
    cellEditProp(){return {
        mode:'dbclick',
        blurToSave:true,
        afterSaveCell:this.onAfterSaveCell
    }}
    componentDidMount() {
      this.props.dispatch(orderActions.getAll());
    }

    render() {
        const { user,orders } = this.props;
        return (<div>
            <div>
                <h1>Hi {user.username}!</h1>
                <p>You're logged in !!</p>
                <p>
                    <Link to="/login">Logout</Link>
                </p>
            </div>
            <div>
                 <h3>Orders list:</h3>
                {orders.loading && <em>Loading orders...</em>}
                {orders.error && <span className="text-danger">ERROR: {orders.error}</span>}
                <BootstrapTable data={ orders.items } pagination cellEdit={ this.cellEditProp() } >
                    <TableHeaderColumn dataField='timeAndDate' editable={ false } width='180'>Time</TableHeaderColumn>
                    <TableHeaderColumn dataField='origin' editable={ false } >Origin</TableHeaderColumn>
                    <TableHeaderColumn dataField='destination' editable={ false }>Destination</TableHeaderColumn>
                    <TableHeaderColumn dataField='orderStatus' filterFormatted dataFormat={ enumFormatter } formatExtraData={ statusType } filter={
                        { type: 'SelectFilter', options: statusType } } editable={{type:'select',options:{values:statusSelectionValues}}}>Order status</TableHeaderColumn>
                     <TableHeaderColumn dataField='farePrice' width='60' editable={ false } dataFormat={currencyFormatter}>Price</TableHeaderColumn>

                    <TableHeaderColumn width='80' dataField='orderID'  dataFormat={linkFormatter} isKey>Details</TableHeaderColumn>
                </BootstrapTable>
            </div></div>
        );
    }
}

const mapStateToProps=(state)=> {
    const { authentication,orders} = state;
    const { user } = authentication;
    return {
        user ,
        orders
    };
}

const connectedDash = connect(mapStateToProps)(Dashboard);
export { connectedDash as Dashboard };
