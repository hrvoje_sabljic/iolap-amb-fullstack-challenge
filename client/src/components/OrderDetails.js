import React from 'react';
import { connect } from 'react-redux';
import { orderActions,pricingActions} from '../actions';
import { Link } from 'react-router-dom';


class OrderDetails extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(orderActions.get(this.props.match.params.orderId));
      }

    render() {
        const {order} =this.props;
        return (
            <div className="col-md-6 col-md-offset-3">
                <h2 style={{display:'inline-block'}}>Order details</h2><h4 style={{display:'inline-block',float:'right'}}> <Link to="/orders" >Orders list</Link></h4>
                <p><b>Order ID:</b> {order.orderID}</p>
                <p><b>Order Status:</b> {order.orderStatus}</p>
                <p><b>Order time:</b> {order.timeAndDate}</p>
                <p><b>Pickup address:</b> {order.origin}</p>
                <p><b>Destination:</b> {order.destination}</p>
                <p><b>Fare price:</b> {order.farePrice}</p>
                <p><b>Phone number:</b> {order.phoneNumber}</p>
                <p><b>Email:</b> {order.email}</p>
                <p><b>Distance:</b> {order.distance}</p>
                <p><b>Expected duration:</b> {order.duration}</p>
            </div>
    );
    }
}

const mapStateToProps=(state)=> {
    const { order } = state;
    return {
        order
    };
}

const connectedOrderDetails = connect(mapStateToProps)(OrderDetails);
export { connectedOrderDetails as OrderDetails };
