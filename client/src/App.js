import React, {Component} from 'react';
import {Router,Route} from 'react-router-dom';
import {connect} from 'react-redux';
import './App.css';
import { LoginPage,PrivateRoute, Dashboard, OrderForm, OrderDetails } from './components';
import {history} from './helpers/history'
import {alertActions} from './actions'

class App extends Component {
  constructor(props) {
    super(props);
    const { dispatch } = this.props;

    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear());
  });
}
render() {
  const { alert } = this.props;
  return (
      <div className="jumbotron">
          <div className="container">
              <div className="col-md-8 col-md-offset-2">
                  {alert.message &&
                      <div className={`alert ${alert.type}`}>{alert.message}</div>
                  }
                  <Router history={history}>
                      <div>
                        <Route exact path="/" component={OrderForm} />
                        <PrivateRoute exact path='/orders' component={Dashboard}></PrivateRoute>
                        <PrivateRoute exact path='/orders/:orderId' component={OrderDetails}></PrivateRoute>
                        <Route path="/login" component={LoginPage} />
                      </div>
                  </Router>
              </div>
          </div>
      </div>
  );
}
}
function mapStateToProps(state) {
  const { alert } = state;
  return {
      alert
  };
}

const connectedApp = connect(mapStateToProps)(App);
export {connectedApp as App};
