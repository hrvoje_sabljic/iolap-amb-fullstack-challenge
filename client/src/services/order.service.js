import {
  authHeader
} from '../helpers';


const handleResponse = response => {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        window.location.replace('/users/login')
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}

const get = id => {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  }

  return fetch(`/orders/${id}`, requestOptions).then(handleResponse)
}

const getAll = () => {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  }

  return fetch(`/orders/`, requestOptions).then(handleResponse)
}

const update = order => {
  const _headers = authHeader();
  _headers['Content-Type'] = 'application/json';
  const requestOptions = {
    method: 'POST',
    headers: _headers,
    body: JSON.stringify(order)
  }

  return fetch(`/orders/update`, requestOptions).then(handleResponse)
}

const create = order => {
  const _headers = authHeader();
  _headers['Content-Type'] = 'application/json';
  const requestOptions = {
    method: 'POST',
    headers: _headers,
    body: JSON.stringify(order)
  }

  return fetch(`/orders/create`, requestOptions).then(handleResponse)
}

export const orderService = {
  get,
  getAll,
  update,
  create
}
