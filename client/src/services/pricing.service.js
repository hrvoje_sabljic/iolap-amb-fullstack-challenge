import {
  authHeader
} from '../helpers';


const handleResponse = response => {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        window.location.replace('/login')
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}

const get = () => {
  const requestOptions = {
    method: 'GET'
  }

  return fetch(`/pricing`, requestOptions).then(handleResponse)
}

const update = order => {
  const _headers = authHeader();
  _headers['Content-Type'] = 'application/json';
  const requestOptions = {
    method: 'POST',
    headers: _headers,
    body: JSON.stringify(order)
  }

  return fetch(`/pricing/update`, requestOptions).then(handleResponse)
}

export const pricingService = {
  get,
  update
}
