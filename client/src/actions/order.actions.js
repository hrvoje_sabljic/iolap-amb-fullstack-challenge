import {
  orderService
} from '../services';
import {
  orderConstants
} from '../constants';
import {
  alertActions
} from './alert.actions';

const gds = new window.google.maps.DistanceMatrixService();
const handleGeoCalculation = async (origin, destination, next) => {
  gds.getDistanceMatrix({
    origins: [origin],
    destinations: [destination],
    travelMode: 'DRIVING'
  }, async (response, status) => {
    let result = response.rows[0].elements[0];
    if (result.hasOwnProperty('distance') === true) {
      next(result)
    } else {
      next(null);
    }
  })
}

const get = id => {
  return dispatch => {
    dispatch({
      type: orderConstants.GET_ORDER
    });

    orderService.get(id)
      .then(async (order) => {
        handleGeoCalculation(order.origin, order.destination, result => {
          if (result) {
            order['distance'] = result.distance.text;
            order['duration'] = result.duration.text;
          }
          dispatch({
            type: orderConstants.GET_ORDER_SUCCESS,
            order
          });
        })

      }, error => {
        dispatch({
          type: orderConstants.GET_ORDER_FAILURE,
          error
        });
      })
  }
}

const getAll = () => {
  return dispatch => {
    dispatch({
      type: orderConstants.GET_ALL_ORDERS
    });

    orderService.getAll()
      .then(orders => {
        dispatch({
          type: orderConstants.GET_ALL_ORDERS_SUCCESS,
          orders
        });
      }, error => {
        dispatch({
          type: orderConstants.GET_ALL_ORDERS_FAILURE,
          error
        });
      })
  }
}

const update = order => {
  return dispatch => {
    dispatch({
      type: orderConstants.UPDATE_ORDER_STATUS
    });

    orderService.update(order)
      .then(order => {
        dispatch({
          type: orderConstants.UPDATE_ORDER_STATUS_SUCCESS,
          order
        });
        dispatch(alertActions.success(`Order: ${order.orderID} status updated! `));
      }, error => {
        dispatch({
          type: orderConstants.UPDATE_ORDER_STATUS_FAILURE,
          error
        });
      })
  }
}

const create = order => {
  return dispatch => {
    dispatch({
      type: orderConstants.CREATE_ORDER
    });

    orderService.create(order)
      .then(order => {
        dispatch({
          type: orderConstants.CREATE_ORDER_STATUS_SUCCESS,
          order
        });
        dispatch(alertActions.success('Order submited!'));
      }, error => {
        dispatch({
          type: orderConstants.CREATE_ORDER_STATUS_FAILURE,
          error
        });
      })
  }
}

export const orderActions = {
  get,
  getAll,
  update,
  create
}
