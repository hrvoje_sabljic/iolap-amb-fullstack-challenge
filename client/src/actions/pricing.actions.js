import {pricingService} from '../services';
import {pricingConstants} from '../constants';


const get = () => {
  return dispatch=>{
    dispatch({type:pricingConstants.GET_PRICING});

    pricingService.get()
    .then(pricing=>{
      dispatch({type:pricingConstants.GET_PRICING_SUCCESS,pricing});
    },error=>{
      dispatch({type:pricingConstants.GET_PRICING_FAILURE,error});
    })
  }
}

const update = pricing => {
  return dispatch=>{
    dispatch({type:pricingConstants.UPDATE_ORDER_STATUS});

    pricingService.update(pricing)
    .then(pricing=>{
      dispatch({type:pricingConstants.UPDATE_PRICING_SUCCESS,pricing});
    },error=>{
      dispatch({type:pricingConstants.UPDATE_PRICING_FAILURE,error});
    })
  }
}

export const pricingActions = {
  get,
  update
}
