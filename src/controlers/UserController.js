const User = require('../models').User;
const winsoton = require('../../logger/winston');
const jwt = require('jsonwebtoken');
class UserController {
  /**
   *
   * @param {*} req
   * @param {*} res
   */
  create(req, res) {
    User.create({
      username: req.body.username,
      password: req.body.password,
    }).then(user => {
      res.status(201).send(user);
    }).catch(ex => {
      winsoton.error(ex);
      res.status('400').send('Error occured');
    })
  }

  /**
   *
   * @param {*} req
   * @param {*} res
   */
  login(req, res) {
    let username = req.body.username;
    let password = req.body.password;

    User.findOne({
      where: {
        username: username
      }
    }).then(user => {
      if (!user) {
        res.redirect('/login');
      } else if (!user.validPassword(password)) {
        res.redirect('/login');
      } else {
        let data = {
          username: user.username,
          id: user.id
        }
        let token = jwt.sign(data, process.env.SECRET_KEY, {
          expiresIn: 60 * 60 * 1000
        });

        data['token'] = token;
        res.status(200).send(JSON.stringify(data));
      }
    })
  }

  delete() {
    throw new Error("Not implemented!");
  }
  get() {
    throw new Error("Not implemented!");
  }

  update() {
    throw new Error("Not implemented!");
  }
}

module.exports = new UserController();
