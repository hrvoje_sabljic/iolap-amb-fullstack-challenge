const Order = require('../models').Order;
const winsoton = require('../../logger/winston');

class OrderController {

  create(req, res) {
    let body = req.body;
    let order = {
      orderStatus: body.orderStatus||'Requested',
      timeAndDate: body.timeAndDate,
      origin: body.origin,
      destination: body.destination,
      farePrice: body.farePrice,
      phoneNumber: body.phoneNumber,
      email: body.email
    }
    Order.create(order).then(order => {
      res.status(201).send(order);
    }).catch(ex => {
      res.status('400').send('Error occured');
    })
  }

  delete(req, res) {
    throw new Error("Not implemented!");
  }

  get(req, res) {
    let orderId = req.params.orderId;
    Order.findOne({
      where: {
        orderId: orderId
      }
    }).then(order => {
      res.status(200).send(order);
    }).catch(ex => {
      res.status('400').send('Error occured');
    })
  }

  getAll(req, res) {
    Order.findAll().then(orders => {
      res.status(200).send(orders);
    }).catch(ex => {
      res.status('400').send('Error occured');
    })
  }

  updateStatus(req, res) {
    let body = req.body;

    Order.findOne({
      where: {
        orderID: body.orderID
      }
    }).then(order => {
      order.orderStatus = body.orderStatus;
      order.save().then((_order) => {
        res.status(200).send(_order);
      }).catch(ex => {
        res.status('400').send('Error occured');
      })
    })
  }
}

module.exports = new OrderController();
