const Pricing = require('../models').Pricing;
const winsoton = require('../../logger/winston');

class PricingController {

  create(req, res) {
    let body = req.body;
    let pricingObj = {
      ik:'',
      basePrice: body.basePrice,
      additionalMileCost: body.additionalMileCost
    };
    Pricing.create(pricingObj).then(_pricing => {
      res.status(201).send(pricing);
    }).catch(ex => {
      res.status('400').send('Error occured');
    })
  }

  get(req, res) {
    Pricing.findOne({
      where: {
        ik: ''
      }
    }).then(pricing => {
      res.status(200).send(pricing);
    }).catch(ex => {
      res.status('400').send('Error occured');
    })
  }

  update(req, res) {
    let body = req.body;

    Pricing.update({
      basePrice: body.basePrice*1,
      additionalMileCost: body.additionalMileCost*1
    }, {
      where: {
        ik: ''
      }
    }).then(status => {
      res.status(200).send('ok');
    }).catch(ex => {
      res.status('400').send('Error occured');
    })

   /*  Pricing.findOne({
      where: {
        ik: ''
      }
    }).then(pricing => {
      pricing.additionalMileCost = body.additionalMileCost,
        pricing.basePrice = body.basePrice;
      pricing.save((_pricing) => {
        res.status(200).send(_pricing);
      }).catch(ex => {
        res.status('400').send('Error occured');
      })
    }).catch(ex => {
      res.status('400').send('Error occured');
    }) */
  }
}

module.exports = new PricingController();
