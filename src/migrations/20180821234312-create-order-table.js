'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.createTable('Orders', {
      orderID: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1,
        primaryKey: true
      },
      orderStatus: {
        type: Sequelize.ENUM('Requested', 'Declined', 'In progress', 'Done'),
        allowNull: false
      },
      timeAndDate: {
        type: Sequelize.DATE,
        allowNull: false
      },
      origin: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: false
        }
      },
      destination: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: false
        }
      },
      farePrice: {
        type: Sequelize.DECIMAL,
        allowNull: false
      },
      phoneNumber: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: false
        }
      },
      email: {
        type: Sequelize.STRING,
        allowNull: true,
        validate: {
          isEmail: true
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Orders');
  }
};
