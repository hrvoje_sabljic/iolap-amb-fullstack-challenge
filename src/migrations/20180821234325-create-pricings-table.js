'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Pricings', {
      ik: {
        type: Sequelize.ENUM(''),
        primaryKey: true,
        allowNull: false
      },
      basePrice: {
        type: Sequelize.DECIMAL,
        allowNull: false,
        defaultValue: 3.0
      },
      additionalMileCost: {
        type: Sequelize.DECIMAL,
        allowNull: false,
        defaultValue: 1.0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Orders');
  }
};
