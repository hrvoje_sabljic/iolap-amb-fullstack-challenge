'use strict';
module.exports = (sequelize, DataTypes) => {
  let Pricing = sequelize.define('Pricing', {
    ik: {
      type: DataTypes.ENUM(''),
      primaryKey: true,
      allowNull: false
    },
    basePrice: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: 3.0
    },
    additionalMileCost: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: 1.0
    }
  });

  return Pricing;
};
