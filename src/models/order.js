module.exports = (sequelize, DataTypes) => {
  let Order = sequelize.define('Order', {
    orderID: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV1,
      primaryKey: true
    },
    orderStatus: {
      type: DataTypes.ENUM('Requested', 'Declined', 'In progress', 'Done'),
      allowNull: false
    },
    timeAndDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    origin: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: false
      }
    },
    destination: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: false
      }
    },
    farePrice: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: false
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        isEmail: true
      }
    }
  });

  return Order;
};
