const router = require('express').Router();
const userController = require('../controlers/UserController');
const jwt = require('jsonwebtoken');

let jwtVerifyLogin = (req, res, next) => {
  let token = req.body.token || req.headers['authorization'];
  if (token) {
    jwt.verify(token, process.env.SECRET_KEY, err => {
      if (err) {
        console.log('Token verification failed, proceed to login');
        next();
      } else {
        res.redirect('/');
      }
    });
  } else {
    console.log('Not token proceed to login');
    next();
  }
};

router.post('/create', userController.create);
router.post('/login', jwtVerifyLogin, userController.login);
module.exports = router;
