const router = require('express').Router();
const pricingController = require('../controlers/PricingController');
const jwt = require('jsonwebtoken');

let jwtVerify = (req, res, next) => {
  let token = req.body.token || req.headers['authorization'];
  if (token) {
    jwt.verify(token, process.env.SECRET_KEY, err => {
      if (err) {
        console.log('Token verification failed, proceed to login');
        res.status(403).send('Unauthorized action');
      } else {
        next();
      }
    });
  } else {
    res.status(403).send('Unauthorized action');
  }
};


router.get('/', pricingController.get);
router.post('/create', jwtVerify, pricingController.create);
router.put('/update', jwtVerify, pricingController.update);

module.exports = router;
