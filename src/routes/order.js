const router = require('express').Router();
const orderController = require('../controlers/OrderController');
const jwt = require('jsonwebtoken');

let jwtVerify = (req, res, next) => {
  let token = req.body.token || req.headers['authorization'];

  if (token) {
    jwt.verify(token, process.env.SECRET_KEY, err => {
      if (err) {
        console.log('Token verification failed, proceed to login');
        res.status(403).send('Unauthorized action');
      } else {
        next();
      }
    });
  } else {
    res.status(403).send('Unauthorized action');
  }
};

router.post('/create', orderController.create);
router.post('/update', jwtVerify, orderController.updateStatus);
router.get('/', jwtVerify, orderController.getAll);
router.get('/:orderId', jwtVerify, orderController.get);

module.exports = router;
